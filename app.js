const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 2001;

// Allows us to read json data
app.use(express.json());

// Allows us to read data from forms
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://admin1:admin123@zuitt-bootcamp1.l5iqt9p.mongodb.net/S36-Activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

mongoose.connection.once("open",() => console.log('Now connected to the database!'));

app.use("/tasks", taskRoute)


/*
db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));
*/

// Listen to the port
app.listen(port, () => console.log(`Now listening to port ${port}`));

