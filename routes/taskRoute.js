	const express = require("express");
	const router = express.Router();

	const taskController = require("../controllers/taskController");

	// [ SECTION ] Get all tasks
	router.get("/",(req, res) => {
		taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
	});

	// [ SECTION ] Create a new task
	router.post("/", (req,res) => {

		taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
	})

	// [ SECTION ] delete a task
	router.delete("/:id", (req,res) => {
		taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
	})

	// [ SECTION ] update a task
	router.put("/:id", (req,res) => {
		taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
	})

	//Activity S36
	
	router.get("/:id",(req, res) => {
		taskController.getTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	});

	router.put("/:id/complete",(req, res) => {
		taskController.getTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	});


	module.exports = router;